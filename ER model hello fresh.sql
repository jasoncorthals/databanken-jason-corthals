use ER;

create table Klant (
	Email varchar (100) not null,
    Id int auto_increment primary key);
    
create table Gerecht (
	Naam varchar (100) not null,
    Bereidingstijd int not null,
    Kostprijs int not null,
    Id int auto_increment primary key);
    
create table Creditcardnummer (
	Nummer char (12) not null,
    Klant_Id int);

alter table Creditcardnummer
add constraint fk_Creditcardnummer_Klant
foreign key (Klant_Id)
references Klant(Id);

create table Bestelling (
	Klant_Id int,
    Gerecht_Id int,
    Besteldatum date not null,
    Leveringsdatum date);

alter table Bestelling
add constraint fk_Bestelling_Klant
foreign key (Klant_Id)
references Klant(Id);

alter table Bestelling
add constraint fk_Bestelling_Gerecht
foreign key (Gerecht_Id)
references Gerecht(Id);