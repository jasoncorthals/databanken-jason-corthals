use ModernWays;

select Verschijningsdatum, Titel, Voornaam, Familienaam
from Boeken
order by Verschijningsdatum DESC, Familienaam ASC;