use Modernways;

insert into Uitleningen(Leden_Id,Boeken_ID,Startdatum,Einddatum)
select Leden.Id, Boeken.Id, '2019-02-01', '2019-02-15'
from Leden join Boeken
where Leden.Voornaam = 'Max' and Boeken.Titel = 'Norwegian Wood';

insert into Uitleningen(Leden_Id,Boeken_ID,Startdatum,Einddatum)
select Leden.Id, Boeken.Id, '2019-02-16', '2019-03-02'
from Leden join Boeken
where Leden.Voornaam = 'Bavo' and Boeken.Titel = 'Norwegian Wood';

insert into Uitleningen(Leden_Id,Boeken_ID,Startdatum,Einddatum)
select Leden.Id, Boeken.Id, '2019-02-16', '2019-03-02'
from Leden join Boeken
where Leden.Voornaam = 'Bavo' and Boeken.Titel = 'Pet Sematary';

insert into Uitleningen(Leden_Id,Boeken_ID,Startdatum)
select Leden.Id, Boeken.Id, '2019-05-01'
from Leden join Boeken
where Leden.Voornaam = 'Yannick' and Boeken.Titel = 'Pet Sematary';