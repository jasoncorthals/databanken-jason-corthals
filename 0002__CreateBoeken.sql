USE Modernways; 
DROP TABLE IF EXISTS `Boeken`;
CREATE TABLE Boeken(
    Voornaam nvarchar(50),
    Familienaam nvarchar(80),
    Titel nvarchar(255),
    Stad nvarchar(50),
    -- alleen het jaartal, geen datetime
    -- omdat de kleinste datum daarin 1753 is
    -- varchar omdat we ook jaartallen kleiner dan 1000 hebben
    Verschijningsdatum char(4),
    Uitgeverij nvarchar(80),
    Herdruk char(4),
    Commentaar TEXT
);