use Modernways;
create table Boeken
(
   Id integer not null primary key,
   Voornaam varchar(25) null,
   Familienaam varchar(50) not null,
   Titel varchar(255) not null,
   Stad varchar(100) null,
   Uitgeverij varchar(100) null,
   Verschijningsdatum char(4) null,
   Herdruk char(4) null,
   Commentaar varchar(500) null
);
/* delete from Boeken where Titel = 'De stad van God';

select * from Boeken;

select Titel, Voornaam, Familienaam from Boeken order by Titel;

select Titel, Voornaam, Familienaam from Boeken order by Familienaam, Titel;

-- om alleen de initalen te tonen, we starten
-- met het eerste karakter en we nemen 1 karakter.
-- Begint niet met 0, zoals je dat verwacht
-- bij een array string in C#
select Titel, substring(Voornaam, 1, 1),
   substring(Familienaam, 1, 1)
   from Boeken order by Familienaam, Titel;

-- om de initialen en een punt erachter te tonen
-- daarvoor gebruiken we 'string concatenation',
-- het aan elkaar plakken van strings
select Titel, substring(Voornaam, 1, 1) + '.' from Boeken order by Titel, Voornaam;

insert into Boeken(
   Voornaam,
   Familienaam
)
values(
   'Aurelius',
   'Augustinus'
); */