use modernways;

select Leden.Voornaam, Taken.Omschrijving
  from Taken
  left join Leden
  on Taken.Leden_Id = Leden.Id;