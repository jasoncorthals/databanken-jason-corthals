use modernways;

ALTER TABLE Tweets
add column Users_id int not null,
add FOREIGN KEY fk_Tweets_Users(Users_Id) 
REFERENCES Users(Id);
