use Modernways;
/*select * from boeken where categorie = 'wiskunde' and Familienaam != 'Gowers' order by verschijnigsdatum;*/SELECT 
select * FROM
    boeken
WHERE
    categorie = 'wiskunde'
        AND NOT Familienaam = 'Gowers'
ORDER BY verschijnigsdatum;