drop database if exists ER;
create database ER;
use ER;

CREATE TABLE Acteur (
    Naam VARCHAR(100),
    Geboortedatum VARCHAR(100) NOT NULL,
    id INT AUTO_INCREMENT PRIMARY KEY
);
    
CREATE TABLE Gebruiker (
    Username VARCHAR(100)
);
    
CREATE TABLE Film (
    Releasedate DATE,
    Titel VARCHAR(100) PRIMARY KEY
);
    
CREATE TABLE Aflevering (
    Releasedate DATE,
    Nummer INT,
    Serie_id INT,
    Nummer INT,
    PRIMARY KEY (Nummer , Serie_Id)
);
    
CREATE TABLE Serie (
    Titel VARCHAR(100) PRIMARY KEY
);
    
CREATE TABLE AppUser (
    Username VARCHAR(100) PRIMARY KEY
);
    
alter table Afleveing 
	add constraint fk_Aflevering_Serie
	foreign key (Serie_Titel)
	references Serie(Titel);

CREATE TABLE SerieGenre (
    Naam VARCHAR(100) PRIMARY KEY,
    Serie_Titel VARCHAR(100)
);

alter table SerieGenre
	add constraint fk_SerieGenre_Serie
    foreign key (Serie_Titel)
    references Serie(Titel);

CREATE TABLE FilmGenre (
    Naam VARCHAR(100) PRIMARY KEY,
    Film_Titel VARCHAR(100)
);

alter table FilmGenre
	add constraint fk_FilmGenre_Film
    foreign key (Film_Titel)
    references Film(Titel);

CREATE TABLE Acteur_speelt_in_Film (
    Acteur_id INT,
    Film_Titel VARCHAR(100)
);
    
alter table  Acteur_speelt_in_Film 
	add constraint fk_Acteur_speelt_in_Film_Acteur
    foreign key (Acteur_Id)
    references Acteur(Id);
    
alter table  Acteur_speelt_in_Film 
	add constraint fk_Acteur_speelt_in_Film_Film
    foreign key (Film_Titel)
    references Film(Titel);

CREATE TABLE Acteur_speelt_in_Film (
    Acteur_Id INT,
    Aflevering_Nummer INT,
    Aflevering_Serie VARCHAR(100)
);
    
alter table  Acteur_speelt_in_Aflevering 
	add constraint fk_Acteur_speelt_in_Aflevering_Acteur
    foreign key (Acteur_Id)
    references Acteur(Id);
    
alter table  Acteur_speelt_in_Aflevering 
	add constraint fk_Acteur_speelt_in_Aflevering_Aflevering
    foreign key (Aflevering_Nummer, Serie, Aflevering_Serie)
    references Aflevering(Nummer, Serie, Titel);

CREATE TABLE AppUser_heeft_gezien_Film (
    id INT PRIMARY KEY,
    AppUser_Username VARCHAR(100),
    Film_Titel VARCHAR(100),
    Rating INT
);
    
alter table AppUser_heeft_gezien_Film 
	add constraint fk_AppUser_heeft_gezien_Film_AppUser
    foreign key (AppUser_Username)
    references AppUser(Username);
    
alter table AppUser_heeft_gezien_Film 
	add constraint fk_AppUser_heeft_gezien_Film_Film
    foreign key (Film_Titel)
    references Film(Titel);

CREATE TABLE AppUser_heeft_gezien_Film_data (
    AppUser_heeft_gezien_Film_Id INT,
    Datum date);

alter table AppUser_heeft_gezien_Film_data
	add constraint fk_AppUser_heeft_gezien_Film_data_AppUser_heeft_gezien_Aflevering_data
    foreign key (AppUser_heeft_gezien_Film_Id)
    references AppUser_heeft_gezien_Film (Id);