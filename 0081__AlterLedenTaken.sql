use modernways;

ALTER TABLE leden
DROP FOREIGN KEY fk_Leden_Taken,
drop column Taken_id;

alter table Taken
add column Leden_id int,
add foreign key fk_Taken_Leden(Leden_id)
references Leden(id);