use Modernways;
delete from boeken where Familienaam = 'Bloch' or Familienaam = 'Gowers';
select * from modernways.boeken;

/*COLLATE*/

/*delete from boeken where Familienaam collate/COLLATE utf8mb4_0900 = 'Bloch' or Familienaam = 'Gowers';*/

/*select from boeken where Familienaam = 'Bloch' or Familienaam = 'Gowers';*/

/* collation = set van regels om tekens met elkaar te vergelijken vb. hoofd en kleine letters, komma's */ 
/* ci = case insensitive, ai = accent insensitive,, cs = case sensitive,...) */ 