/*JI
-- 13 november 2012
-- voeg een not null contraint toe op de kolom Familienaam
-- Bestandsnaam:
-- BoekenAlterFamilienaamAlterNotNull.sql*/

use Modernways;
alter table Postcodes change code not null;