use ModernWays;

SELECT 
    *
FROM
    boeken
WHERE
    (boeken.Stad IN ('Amsterdam' , 'Utrecht'))
        AND (boeken.Categorie IN ('Wiskunde' , 'Filosofie'));