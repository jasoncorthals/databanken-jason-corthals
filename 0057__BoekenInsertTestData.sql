use modernways;
insert into Boeken(Voornaam,Familienaam,Titel)
values
('Haruki','Murakami','Kafka on the Shore'),
('Haruki','Murakami','Norwegian Wood'),
('Haruki','Murakami','1Q84'),
('Haruki','Murakami','Hard-Boiled Wonderland and the End of the World'),
('Haruki','Murakami','After the Quake'),
('David','Mitchell','Cloud Atlas'),
('David','Mitchell','Number9Dream'),
('David','Mitchell','The 1000 Autumns of Jacob De Zoet'),
('Nick','Harkaway','The Gone-Away World'),
('Nick','Harkaway','Angelmaker'),
('Thomas','Ligotti','Teatro Grottesco'),
('Thomas','Ligotti','Teatro Grottesco');