use modernways;

select Leden.Voornaam, Taken.Omschrijving
from Leden
left join Taken
on Leden.Id 
where Taken.Leden_Id is null

UNION

select Leden.Voornaam, Taken.Omschrijving
from Leden
right join Taken
on Taken.Leden_Id
where Leden.Id is null;