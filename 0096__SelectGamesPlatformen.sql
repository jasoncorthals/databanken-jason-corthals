use modernways;

select Games.Titel, Platformen.Naam
from Releases
     inner join Platformen on Releases.Platformen_Id = Platformen.Id
     inner join Games on Releases.Games_Id = Games.Id