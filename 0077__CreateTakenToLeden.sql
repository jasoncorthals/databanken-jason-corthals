use ModernWays;

create table TakenToLeden (Taken_id int not null, 
							Leden_id int not null);

ALTER TABLE TakenToLeden  
add FOREIGN KEY fk_TakenToLeden_Taken(Taken_Id) 
REFERENCES Taken(Id);
ALTER TABLE TakenToLeden  
add FOREIGN KEY fk_TakenToLeden_Leden(Leden_Id) 
REFERENCES Taken(Id);
insert into TakenToLeden(Taken_id, Leden_id)
values
(1,2),
(2,1),
(3,3);