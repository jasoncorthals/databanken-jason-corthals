use modernways;

SELECT 
    Games.Titel, Platformen.Naam
FROM
    (Games
    LEFT JOIN Releases ON Releases.Games_Id = Games.Id)
        LEFT JOIN
    Platformen ON Releases.Platformen_Id = Platformen.Id
WHERE
    Releases.Games_Id IS NULL 
UNION SELECT 
    Games.Titel, Platformen.Naam
FROM
    (Platformen
    LEFT JOIN Releases ON Releases.Platformen_Id = Platformen.Id)
        LEFT JOIN
    Games ON Releases.Games_Id = Games.Id
WHERE
    Releases.Platformen_Id IS NULL;