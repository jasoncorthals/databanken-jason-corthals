use ModernWays;
drop table if exists Personen;

CREATE TABLE Personen (
    Voornaam VARCHAR(255) not null, 
    Familienaam varchar(255) not null, 
    Email varchar (255) not null, 
    Id INT AUTO_INCREMENT PRIMARY KEY
);