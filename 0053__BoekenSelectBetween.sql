use ModernWays;

SELECT 
    boeken.voornaam, boeken.familienaam, boeken.titel
FROM
    boeken
WHERE
    boeken.Verschijningsdatum BETWEEN '1990' AND '2011'
        AND boeken.familienaam LIKE BINARY 'B%'
ORDER BY boeken.titel;