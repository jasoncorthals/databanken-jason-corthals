use Modernways;
insert into Boeken (
   Boeken.Id, 
   Boeken.Voornaam,
   Boeken.Familienaam,
   Boeken.Titel,
   Boeken.Stad,
   Boeken.Uitgeverij,
   Boeken.Verschijningsdatum,
   Boeken.Herdruk,
   Boeken.Commentaar,
   Boeken.Categorie
)
values 
('1','Samuel', 'Ijsseling', 'Heidegger. Denken en Zijn. Geven en Danken', 'Amsterdam', '', '2014', '', 'Nog te lezen', 'Filosofie'),
('2','Jacob', 'Van Sluis', 'Lees wijzer bij Zijn en Tijd', '', 'Budel', '1998', '', 'Goed boek', 'Filosofie');