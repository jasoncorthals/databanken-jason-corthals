use ModernWays;

SELECT 
    boeken.voornaam,
    boeken.familienaam,
    boeken.titel,
    boeken.verschijningsdatum
FROM
    boeken
WHERE
    boeken.Verschijningsdatum BETWEEN '1990' AND '2011'
        AND (boeken.familienaam LIKE '%B%'
        OR boeken.familienaam LIKE '%A%'
        OR boeken.familienaam LIKE '%F%')
ORDER BY boeken.verschijningsdatum;