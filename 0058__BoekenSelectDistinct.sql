use ModernWays;

SELECT DISTINCT
    boeken.Familienaam, boeken.Voornaam
FROM
    Boeken
WHERE
    (boeken.Familienaam LIKE '%a%' OR '%e%')
ORDER BY Familienaam , Voornaam;