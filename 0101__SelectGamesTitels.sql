use modernways;

SELECT 
    Games.Titel, Platformen.Naam
FROM
    (Games
    LEFT JOIN Releases ON Releases.Games_Id = Games.Id)
        LEFT JOIN
    Platformen ON Releases.Platformen_Id = Platformen.Id;