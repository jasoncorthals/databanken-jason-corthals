﻿using System;
using System.IO;
using System.Diagnostics;

namespace Databanken
{
    class Program
    {

        // vervang door je eigen gebruikersnaam
        static string user = "root";
        // vervang door je eigen wachtwoord
        static string password = "mysql";
        // vervang door de map met je genummerde scripts
        // dit veronderstelt dat er *niets* anders in die map staat!
        static string dir = @"C:\databankensql\databanken-jason-corthals";
        static string mysqlbin = @"C:\Program Files\MySQL\MySQL Server 8.0\bin\mysql";
        static void Main(string[] args)
        {
            Console.WriteLine("Tot welk bestand wens je uit te voeren?");
            int limit = int.Parse(Console.ReadLine());
            string[] files = Directory.GetFiles(dir, "*.sql");
            Array.Sort(files);
            foreach (string fileAbsPath in files)
            {
                string[] components = fileAbsPath.Split("\\");
                string fn = components[components.Length - 1];
                if (Convert.ToInt32(fn.Substring(0, 4),10) <= limit)
                {
                    using (Process mysqlProcess = new Process()) {
                        mysqlProcess.StartInfo.FileName = mysqlbin;
                        mysqlProcess.StartInfo.ArgumentList.Add($"--user={user}");
                        mysqlProcess.StartInfo.ArgumentList.Add($"--password={password}");
                        mysqlProcess.StartInfo.RedirectStandardInput = true;
                        mysqlProcess.Start();
                        mysqlProcess.StandardInput.Write($"source {fileAbsPath}");
                        mysqlProcess.StandardInput.Close();
                        mysqlProcess.WaitForExit();
                    }
                }
            }
        }
    }
}
