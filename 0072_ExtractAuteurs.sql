/*1. Definieer de structuur van de nieuwe entiteit. Vergeet niet te zorgen voor een primaire sleutel. */

use modernways;
drop table if exists Auteurs;

create table Auteurs 
(Voornaam varchar(255),
Familienaam varchar(255),
id int auto_increment primary key 
);

/*2. Vul de tabel voor de nieuwe entiteit in door de afgeleide velden te selecteren uit de (unie van de) oude entiteit(en). Als het niet om een unie gaat, gebruik dan DISTINCT. */

insert into Auteurs(Voornaam, Familienaam) 
select distinct Voornaam,Familienaam 
from Boeken;

/*3. Voeg kolommen voor foreign key(s) toe aan de oude entiteit(en). */

ALTER TABLE Boeken  
add column Auteurs_Id int, 
add FOREIGN KEY fk_Boeken_Auteurs(Auteurs_Id) 
REFERENCES Auteurs(Id);

/*4. Vul de kolommen voor foreign keys in door de data in de oude kolommen te vergelijken met die in de afgeleide kolommen. Let op! Je kan NULL niet geldig vergelijken met iets anders, ook niet met zichzelf. Je kan wel testen of een waarde NULL is met IS NULL. */
/* is null zorgt ervoor dat bij vergelijking van de tabellen null = null en dat dit true teruggeeft ipv een fout*/

UPDATE Boeken,
    Auteurs 
SET 
    Auteurs_Id = Auteurs.Id
WHERE
    ((Auteurs.Voornaam is null and Boeken.Voornaam is null) or 
    (Auteurs.Voornaam = Boeken.Voornaam)) AND
    ((Auteurs.Familienaam is null and Boeken.Familienaam is null) or
	(Auteurs.Familienaam = Boeken.Familienaam));
        
/*5. Voeg de NOT NULL constraint toe aan de foreign keys. */

Alter table Boeken
MODIFY COLUMN Auteurs_id int not null;

/*6. Verwijder de overbodige kolommen uit de oude entiteiten. */

Alter table Boeken
drop Voornaam,
drop Familienaam;















