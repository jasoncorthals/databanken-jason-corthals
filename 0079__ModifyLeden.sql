use ModernWays;

alter table Leden 
add column Taken_id int,
add FOREIGN KEY fk_Leden_Taken(Taken_Id) 
REFERENCES Taken(Id);
insert into Leden(Taken_id)
values
(2),
(1),
(3);