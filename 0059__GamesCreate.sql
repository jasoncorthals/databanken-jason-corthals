use ModernWays;
drop table if exists Games;


CREATE TABLE Games (
    Titel VARCHAR(255),
    Ontwikkelaar VARCHAR(255),
    Id INT AUTO_INCREMENT PRIMARY KEY
);