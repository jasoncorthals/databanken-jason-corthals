use ModernWays;
drop table if exists Ontwikkelaar;


CREATE TABLE Ontwikkelaar (
    Naam VARCHAR(255),
    Id INT AUTO_INCREMENT PRIMARY KEY
);